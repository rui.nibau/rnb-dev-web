#!/bin/bash

set -e

DIR="$( cd "$( dirname "$0" )" && pwd )"

lint() {
    cd $DIR
    eslint src --ext .js
}

doc() {
    cd $DIR
    rm -fr jsdoc/*
    jsdoc -r -c ./jsdoc.json -d ./jsdoc/
}

test() {
    cd $DIR
    node ./tests/run.js
}

build() {
    cd $DIR
    echo "build"
}

deps() {
    echo -e "Cloning dependencies id deps directory..."
    cd $DIR
    # clear
    rm -fr deps/*
    # read deps
    local urls=$(jq -r .deps[] ./package.json)
    # clone
    cd $DIR"/deps"
    for url in ${urls[@]}; do
        git clone $url
    done
    
    # node deps
    npm run install
    
    echo -e "Cloning done."
}

symlinks() {
    echo -e "Creating symlinks..."
    cd $DIR
    # read symlinks
    package=$(< ./template.package.json)
    local linkdirs=($(echo "$package" | jq -r '.symlinks | keys[]'))
    for linkdir in ${linkdirs[@]}; do
        echo -e "  > Creating symlinks in "$linkdir
        cd $DIR
        local targets=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | keys_unsorted[]'))
        local links=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | .[]'))
        # create symlinks
        cd $DIR"/"$linkdir
        for (( i=0; i<${#targets[@]}; i++ )); do
            link=${links[$i]}
            target=${targets[$i]}
            if [[ -f $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            elif [[ -d $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            else
                ln -sf $target $link
                echo -e "    > "$target" -> "$linkdir$link" created."
            fi
        done
    done
}

symlinks_test() {
    # json="{\"a\":\"a\",\"b\":\"b\",\"d\":\"d\",\"c\":\"c\"}"
    # values=$(echo "$json" | jq -r '.[]')
    # keys=$(echo "$json" | jq -r 'keys[]')
    # echo $values
    # echo $keys
    echo -e "Creating symlinks..."
    cd $DIR
    package=$(< ./template.package.json)

    # read symlinks
    local linkdirs=($(echo "$package" | jq -r '.symlinks | keys[]'))
    for linkdir in ${linkdirs[@]}; do
        echo -e "  > Creating symlinks in "$linkdir
        cd $DIR
        local targets=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | keys_unsorted[]'))
        local links=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | .[]'))
        # create symlinks
        cd $DIR"/"$linkdir
        for (( i=0; i<${#targets[@]}; i++ )); do
            link=${links[$i]}
            target=${targets[$i]}
            echo -e $target" -> "$link
        done
    done
}

case $1 in
    deps)
        deps;;
    symlinks)
        symlinks;;
    symlinks-test)
        symlinks_test;;
    setup)
        deps
        symlinks
        ;;
    lint)
        lint;;
    test)
        test;;
    jsdoc)
        doc;;
    build)
        build;;
esac
